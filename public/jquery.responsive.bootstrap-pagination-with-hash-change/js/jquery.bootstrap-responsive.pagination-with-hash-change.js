;(function($, window, undefined) {
    'use strict';

    $.ResponsivePagination = function(options, element) {

        this.obj = $(element),
        this.element = element;

        // fixed internal css for function
        var internal = {
            nextSelector : '.next-repa',
            nextClass : 'next-repa',
            prevSelector : '.prev-repa',
            prevClass : 'prev-repa',
            pageingItemSelector : '.item-repa',
            pageingItemClass : 'item-repa',
            pageingItemLinkSelector : '.link-repa',
            pageingItemLinkClass : 'link-repa',
            activeItemClass : 'active',
            eventTrigger : 'click touch'
        }    

        this.internal = internal;

        $.ResponsivePagination.defaults = {
            counterSelector : '.counter-repa',
            pagingSelector : '.nav-repa',
            itemsPerPage : 5,
            initialPage : 1,
            showPageNav : true,
            showPageNumber : false,
            textCounter : 'Page: ',
            ulClass : 'pagination',
            liClass : 'page-item', 
            aClass : 'page-link',
            prevHtml : '<li class="page-item"><a class="page-link ' + this.internal.prevClass + '" href="">Previous</a></li>',
            nextHtml : '<li class="page-item"><a class="page-link ' + this.internal.nextClass + '" href="">Next</a></li>',
        };

        this.options  = $.extend(true, {}, $.ResponsivePagination.defaults, options);

        this.numItems = this.obj.children().length;
        this.numPages = Math.ceil(this.numItems / options.itemsPerPage);
        this.maxValue = this.numPages;
        this.minValue = 1;

        this._init();
    };


    Range = (function () {

        var i = 0,
            minValue = 0,
            maxValue = 5;

        return {
            get: function(){
                return i;
            },
            set: function (val) {
                i = val;
            },
            setborders: function (min, max) {
                minValue = min;
                maxValue = max;
            },
            plus: function () {
                return (i < maxValue) ? ++i : i = minValue;
            },
            minus: function () {
                return (i > minValue) ? --i : i = maxValue;
            }
        };
    })();


    var Hash = (function () {

        return {
            protocol: '//',
            host: window.location.host,
            pathname: window.location.pathname,
            search: window.location.search,
            set: function (hash) {
                window.location = this.protocol +
                    this.host +
                    this.pathname +
                    this.search +
                    '#' + hash;
            },
            clear: function () {
                // no refresh
                window.location.replace('#')
            },
            get: function () {
                return window.location.hash.replace('#', '');
            }
        };
    })();


    function ViewPage(page,obj,options) {

        obj.children().hide();

        var i,
        s = (page - 1) * options.itemsPerPage,
            max = page * options.itemsPerPage;

        for (i = s; i < max; i += 1) {
            obj.children().eq(i).show();
        }
    }


    function ViewPageNumber(page, options, numPages) {

        if (options.showPageNumber === true){
            $(options.counterSelector).html(options.textCounter + page + '/' + numPages);
        }
    }

 
    // bootstrap version with options ulClass, liClass, aClass
    function ViewPageNav(options, maxValue, internal) {

        var ulClass = ' class="' + options.ulClass + '"',
            liClass = ' class="' + options.liClass + ' ' + internal.pageingItemClass + '"',
            aClass = ' class="' + options.aClass + ' ' + internal.pageingItemLinkClass + '"',
            ListWrapper = '<ul class="' + options.ulClass +  '"></ul>',
            ListElements = '',
            i;

        if (options.showPageNav === true){

            for (i = 1; i <= maxValue; i += 1) {
                ListElements += '<li' + liClass  + '><a href="#' +
                    i +
                    '"' + aClass + '>' +
                    i +
                    '</a></li>';
            }        
        }

        $(options.prevHtml + ListElements + options.nextHtml)
            .appendTo(
                $(ListWrapper)
                    .appendTo(options.pagingSelector)
                );
    }


    function ActiveState(page, options, internal) {

        if (options.showPageNav === false) return;
        var pageingItemSelector = internal.pageingItemSelector;

        $(options.pagingSelector).each(function () {

            $(this).find(pageingItemSelector)
                .removeClass(internal.activeItemClass)
                    .eq(page - 1)
                    .addClass(internal.activeItemClass);
        });
    }


    $.ResponsivePagination.prototype = {

        _init: function() {

            this._registerHandlers(); 

            // set the startpage
            var page = this._setInitalPage(this.options.initialPage); 
            Hash.set(page); 
            Range.setborders(this.minValue, this.maxValue);
            Range.set(page);

            
            ViewPageNav(this.options, this.maxValue, this.internal);

            ViewPage(page, this.obj, this.options); 
            ViewPageNumber(page, this.options, this.maxValue)
            ActiveState(page, this.options, this.internal); 
            
        },


        _registerHandlers: function() {

            var next = this.internal.nextSelector + ', ' + this.internal.eventTrigger;
            var prev = this.internal.prevSelector + ', ' + this.internal.eventTrigger;
            var pagenav = this.internal.pageingItemLinkSelector + ', ' + this.internal.eventTrigger;

            var handlers  = {};
            handlers[next] = '_userActionNext';
            handlers[prev] = '_userActionPrev';
            handlers[pagenav] = '_userActionPageNav';

            var eventdata = {    
                obj: this.obj, 
                options: this.options,
                internal: this.internal,
                numPages: this.numPages
            }

            var _this = this;

            $.each(handlers, function(k, v) {
                var p = k.split(", "),
                    selector = p[0],
                    triggers = p[1];
                $(document).delegate(selector, triggers, eventdata, _this[v]);
                //$(_this.options.pagingSelector).on( triggers, selector, eventdata, _this[v] );
            });
        },

        _userActionNext: function(e) {
           
            e.preventDefault();
            var page = Range.plus();
            Hash.set(page);
            
            ViewPage(page, e.data.obj, e.data.options);
            ViewPageNumber(page, e.data.options, e.data.numPages); 
            ActiveState(page, e.data.options, e.data.internal);
            
        },

        _userActionPrev: function(e) {
           
            e.preventDefault();
            var page = Range.minus();
            Hash.set(page);

            ViewPage(page, e.data.obj, e.data.options);
            ViewPageNumber(page, e.data.options, e.data.numPages);
            ActiveState(page, e.data.options, e.data.internal);
            
        },

        _userActionPageNav: function(e) {
            
            e.preventDefault();
            var page = this.hash.replace('#', '');
            Hash.set(page);
            Range.set(page); 

            ViewPage(page, e.data.obj, e.data.options, e.data.numPages);
            ViewPageNumber(page, e.data.options, e.data.numPages);
            ActiveState(page, e.data.options, e.data.internal);
        },
  

        _setInitalPage: function(initValue) {
           
            return initValue;
        },
        

        destroy: function() {
            
            this.obj.removeData();
            $(document).undelegate( this.internal.prevSelector, 'click touch');
            $(document).undelegate( this.internal.nextSelector, 'click touch');
            $(document).undelegate( this.internal.pagingSelector, 'click touch');
            //$(this.options.pagingSelector).off('click touch');
            $(this.options.pagingSelector).empty();
            $(this.options.counterSelector).empty();
            $(this.element).find('li').removeAttr('style');
            Hash.clear();
            console.log('-- clear one plugin instance of ResponsivePagination --');
        }
    }; 


       $.fn.ResponsivePagination = function(options) {

        this.each(function () {

            var self = $.data(this, 'ResponsivePagination');
            if( typeof options === 'string' ) {
                var args = Array.prototype.slice.call(arguments, 1);
                if ( !self ) {
                        console.log(' -- cannot call methods prior to initialization of ResponsivePagination -- ');
                        console.log(' -- attempted to call method ' + options + ' -- '); 
                    return;
                }

                if( !$.isFunction(self[options]) || options.charAt(0) === '_' ) {
                    console.log(' -- no such public method in plugin ResponsivePagination ' + options + ' -- ');
                    return;
                }

                self[options].apply(self, args);

            } else {
                if(self) {
                    self._init(); 
                    console.log(' -- plugin ResponsivePagination is allready runnig -- ');
                } else {
                    self = $.data(this, 'ResponsivePagination', new $.ResponsivePagination(options, this));
                    console.log(' -- plugin ResponsivePagination initialization -- ');
                }
            } 
        });

        return this;
    }; 
})(jQuery, window);