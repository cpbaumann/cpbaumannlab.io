// https://prepros.io/help/javascript

/*!
  * Bootstrap v4.6.0 (https://getbootstrap.com)
  * Copyright 2011-2021 The Bootstrap Authors (https://github.com/twbs/bootstrap/graphs/contributors)
  * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
  */
//@prepros-append bs_js_source/index.js
//@prepros-append bs_js_source/util.js
//@prepros-append bs_js_source/collapse.js
//@prepros-append bs_js_source/scrollspy.js
//@prepros-append custom_js/bom.js
