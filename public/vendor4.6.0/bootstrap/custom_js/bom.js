(function($) {
  "use strict"; 


    var Hash = (function () {

        return {
            protocol: '//',
            host: window.location.host,
            pathname: window.location.pathname,
            search: window.location.search,
            set: function (hash) {
                window.location = this.protocol +
                    this.host +
                    this.pathname +
                    this.search +
                    '#' + hash;
            },
            clear: function () {
                // no refresh
                window.location.replace('#')
            },
            get: function () {
                return window.location.hash.replace('#', '');
            }
        };
    })();




  $(function () {
    $(window).scroll(function () {
      if ($(this).scrollTop() > 100) { 
        $('#totop').removeClass('off');
      } else {
        $('#totop').addClass('off');
      }
    });
  });


  $('[data-collapse-state]').on('click',function() {
    $('.navbar-collapse').collapse('hide');
  });


  $('body').scrollspy({
    target: '#mainnav',
    offset: 100
  });


  var navbarCollapse = function() {
  	 $("#mainnav").addClass("fixed-top");
    if ( $("#mainnav").offset().top > 100 ) {
      $("#mainnav").addClass("navbar-shrink");
    } else {
      $("#mainnav").removeClass("navbar-shrink");
    }
  };


  if ($('#mainnav')[0]) {

    navbarCollapse();
    $(window).scroll(navbarCollapse);
  }


  var month = function() {
    var d = new Date();
    var n = d.getMonth()+1;
    return n;
  }

  var credits = [];

  credits[7] =  {
            name: "Stefan Steinbauer",
            id: "@usinglight"
        }

  credits[8] = {
            name: "Laura Ramirez",
            id: " @lauramayela99"
        }

  credits[9] = {
            name: "Christian Bisbo Johnsen",
            id: "@christianbisbo.com"
        }

  credits[10] = {
            name: "Wes Carpani",
            id: "@wes_c"
        }

  credits[11] =  {
            name: "Stefan Steinbauer",
            id: "@usinglight"
        }

  credits[12] = {
            name: "Thomas Broeker",
            id: "@thomasbroeker"
        }

  credits[1] = {
            name: "Ben Koorengevel",
            id: "@benkoorengevel"
        }

  credits[2] =  {
            name: "Stefan Steinbauer",
            id: "@usinglight"
        }

  credits[3] = {
            name: "Shifaaz shamoon",
            id: "@sotti"
        }

  credits[4] = {
            name: "Jeremy Bishop",
            id: "@jeremybishop"
        }


  credits[5] =  {
            name: "Stefan Steinbauer",
            id: "@usinglight"
        }

  credits[6] = {
            name: "Laura Ramirez",
            id: " @lauramayela99"
        }



  var getMonth = function() {
    var m;
        
      if ( Hash.get() > 0 ){
          console.log('-- Debug the dynamic bg mode. We see header-bg-' + Hash.get() + ' and contact-section-bg-' + Hash.get() + ' --')
          m = Hash.get();
      }else{
          m = month(); 
      }
      return m;
  }

  $("#start").addClass('header-bg-' + getMonth());
  $("#contact").addClass('contact-section-bg-' + getMonth());
  $("#credit").html('Photo by <a href="https://unsplash.com/' + credits[getMonth()].id +'">' + credits[getMonth()].name + ' </a> on Unsplash');
})(jQuery); 

(function(){
    if ($('#mainNavsub')[0]) {
        var previousScroll = 0;

        $(window).scroll(function(){
          var currentScroll = window.pageYOffset;
          console.log(currentScroll)
          if (currentScroll > previousScroll){
              $('#mainNavsub').removeClass('is-visible');
              $('#mainNavsub').addClass('is-hidden');
          } else {
              $('#mainNavsub').removeClass('is-hidden');
              $('#mainNavsub').addClass('is-visible');
          }
          previousScroll = currentScroll;
        });
    }    
})();